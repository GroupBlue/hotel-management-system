package com.hms.danhms.services;


import com.hms.danhms.entity.BookingDao;
import com.hms.danhms.repository.BookingRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class BookingService {

    BookingRepository bookingRepo;

    public BookingService(BookingRepository bookingRepo) {
        this.bookingRepo = bookingRepo;
    }

    public void placeBooking(BookingDao bookingDao){
        bookingRepo.save(bookingDao);
    }
}
