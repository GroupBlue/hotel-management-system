package com.hms.danhms.services;

import com.hms.danhms.entity.UsersDao;
import com.hms.danhms.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UserService {

    UsersRepository usersRepo;

    public UserService(UsersRepository usersRepo) {
        this.usersRepo = usersRepo;
    }

    public void createUser(UsersDao usersDao){
        usersRepo.save(usersDao);
    }

    public List<UsersDao> allUsers(){
        return usersRepo.findAll();
    }

    //This handles the values gotten from the user and
    //Checks the database is user exists or not
    public String getLoggedUser(String email, String password){
       List<UsersDao> user = usersRepo.findUsersDaoByEmailAddress(email);

       if (user.isEmpty()){
           return "login";
       }else{
           if(email.equalsIgnoreCase(user.get(0).getEmailAddress()) && password.equals(user.get(0).getPassword())){
                return "rooms";
           }
       }
       return "login";
    }
}
