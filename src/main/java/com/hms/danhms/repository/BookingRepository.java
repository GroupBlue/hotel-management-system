package com.hms.danhms.repository;

import com.hms.danhms.entity.BookingDao;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookingRepository extends JpaRepository<BookingDao, Integer> {
}
