package com.hms.danhms.repository;

import com.hms.danhms.entity.UsersDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UsersRepository extends JpaRepository<UsersDao, Integer> {

    @Override
    List<UsersDao> findAll();

    List <UsersDao> findUsersDaoByEmailAddress(String email);


}
