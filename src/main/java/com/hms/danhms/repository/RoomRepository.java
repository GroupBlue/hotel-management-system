package com.hms.danhms.repository;

import com.hms.danhms.entity.RoomDao;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoomRepository extends JpaRepository<RoomDao, String>{
}
