package com.hms.danhms.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity(name = "room_images")
public class RoomDao {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String fileName;
    private String fileType;

    @Lob
    private byte[] image;

    public RoomDao() {}

    public RoomDao(String fileName, String fileType, byte[] image) {
        this.fileName = fileName;
        this.fileType = fileType;
        this.image = image;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
