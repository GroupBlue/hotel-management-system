package com.hms.danhms.controller;


import com.hms.danhms.entity.UsersDao;
import com.hms.danhms.repository.UsersRepository;
import com.hms.danhms.services.UserService;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.bind.BindResult;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class LoginController {

    @Autowired
    UserService userService;

    UsersRepository usersRepo;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(){
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String home(WebRequest webRequest, Model model) {
        String email;
        String password;
        //String rememberMe = null;
        Boolean error = false;

        email = webRequest.getParameter("email");
        password = webRequest.getParameter("password");
       // rememberMe = webRequest.getParameter("rememberMe");

        if (email == "" && password == ""){
            error = false;
            model.addAttribute("error", error);
            return "login";
        }else{
            return userService.getLoggedUser(email, password);
        }

    }

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String signup(Model model){
        model.addAttribute("createUser", new UsersDao());
        return "signup";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String signed(@ModelAttribute ("createUser") UsersDao usersDao, Model model, WebRequest webRequest){
          String name;
           Boolean error = false;
           Boolean user = false;

           name = webRequest.getParameter("name");

        if (usersDao == null) {
            error = true;
            model.addAttribute("error", error);
            return "signup";
        } else {
            user = true;
            System.out.println(usersDao);
            userService.createUser(usersDao);
            model.addAttribute("user", user);
            model.addAttribute("name", name);
            return "login";
        }

    }
}
