package com.hms.danhms.controller;

import com.hms.danhms.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UsersController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/all-users", method = RequestMethod.GET)
    public String allUsers(Model model){
        model.addAttribute("users", userService.allUsers());
        System.out.print(userService.allUsers());
        return "users";
    }
}
