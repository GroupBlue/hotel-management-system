package com.hms.danhms.controller;

import com.hms.danhms.entity.BookingDao;
import com.hms.danhms.entity.RoomDao;
import com.hms.danhms.services.BookingService;
import com.hms.danhms.services.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Date;

@Controller
public class BookingController {

    @Autowired
    BookingService bookingService;

    @RequestMapping( value = "/rooms", method = RequestMethod.POST)
    public String book(@Valid BookingDao bookingDao, Model model){

        Boolean error = false;

        if (bookingDao == null){
            error = true;
            model.addAttribute("errorBooking", error);
            return "index";
        }else{
            bookingService.placeBooking(bookingDao);
            return "rooms";
        }
    }
}
